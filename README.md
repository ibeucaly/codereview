# Go Small Web App for Code Review

コードレビュー用のGoによる小さなWebアプリケーションです。  
Platon研修担当者システムの現実装の一部を切り出して作成しました。

## 動作要件

- Go 1.10
- [dep](https://github.com/golang/dep)
- MySQL

## コードの準備

このリポジトリを`$GOPATH/src`以下にクローンしてください。
```bash
cd $GOPATH/src
git clone git@gogs.logosware.net:yukari.ishibashi/codereview.git
```

## MySQLの準備

MySQLに新しいデータベースを作成してください。  
作成したデータベースのすべての権限を持つユーザを作成してください。  
config_mysql.iniに、MySQLのアドレス、データベース名、ユーザ名、パスワードを書いてください。

## ビルド

```bash
cd $GOPATH/src/codereview
dep ensure
go build
```

## テスト

```bash
go test ./...
```

※repositoryパッケージのテストを実行するには、MySQLにテスト用のデータベースを用意する必要があります。  
設定は、repository/repository_test.goの`init()`に記述してください。

## 実行

データベースを初期化します：
```bash
./codereview initdb
```

Webアプリを起動します（localhost:8080）：
```bash
./codereview
```
