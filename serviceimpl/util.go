package serviceimpl

// diffSlice aに存在するがbに存在しない要素を返す。
func diffSlice(a, b []int) []int {
	mb := map[int]bool{}
	for _, x := range b {
		mb[x] = true
	}
	ab := []int{}
	for _, x := range a {
		if _, ok := mb[x]; !ok {
			ab = append(ab, x)
		}
	}
	return ab
}

// uniqueSlice 重複した要素を除去したスライスを返す。
func uniqueSlice(a []int) []int {
	s := make([]int, 0, len(a))
	m := map[int]bool{}
	for _, x := range a {
		if _, ok := m[x]; !ok {
			s = append(s, x)
			m[x] = true
		}
	}
	return s
}
