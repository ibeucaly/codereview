package serviceimpl

import (
	"strings"

	"github.com/juju/errors"

	"codereview/model"
	"codereview/repository"
	"codereview/service"
)

// UserService service.UserServiceの実装。
type UserService struct {
	*Service
}

// Get ユーザを取得する。
func (s *UserService) Get(c *service.Context, id int) (*model.User, error) {
	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, err
	}
	defer store.Close()

	user, err := store.User().Get(id)
	if err != nil {
		if store.IsRecordNotFoundError(err) {
			return nil, errors.NotFoundf("user#%d", id)
		}
		return nil, errors.Annotatef(err, "failed to get user#%d", id)
	}
	return user, nil
}

// Search ユーザを検索する。
func (s *UserService) Search(c *service.Context, param *service.SearchParam) (
	list []*model.User, total int, err error) {
	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, 0, err
	}
	defer store.Close()

	sp := reposSearchParam(param)
	list, total, err = store.User().Search(sp)
	if err != nil {
		err = errors.Annotate(err, "failed to search users")
	}
	return list, total, err
}

// Create ユーザを作成する。
func (s *UserService) Create(c *service.Context, in *model.User) (*model.User, error) {
	user := &model.User{
		Name: strings.TrimSpace(in.Name),
		Mail: strings.TrimSpace(in.Mail),
	}

	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, err
	}
	defer store.Close()

	// 入力データを検証
	err = validateUser(store, user)
	if err != nil {
		return nil, err
	}

	// ユーザを作成
	err = store.User().Create(user)
	if err != nil {
		return nil, errors.Annotate(err, "failed to create user")
	}
	return user, nil
}

// Update ユーザを更新する。
func (s *UserService) Update(c *service.Context, in *model.User) (*model.User, error) {
	user := &model.User{
		ID:   in.ID,
		Name: strings.TrimSpace(in.Name),
		Mail: strings.TrimSpace(in.Mail),
	}

	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, err
	}
	defer store.Close()

	// ユーザが存在するか確認
	err = userExists(store, user.ID)
	if err != nil {
		return nil, err
	}

	// 入力データを検証
	err = validateUser(store, user)
	if err != nil {
		return nil, err
	}

	// ユーザを更新
	err = store.User().Update(user)
	if err != nil {
		return nil, errors.Annotatef(err, "failed to update user#%d", user.ID)
	}
	return user, nil
}

func validateUser(store repository.Store, user *model.User) error {
	reasons := map[string]string{}
	if user.Name == "" {
		reasons["name"] = "name required"
	}
	if user.Mail == "" {
		reasons["mail"] = "mail required"
	} else {
		dup, err := store.User().DupMail(user.Mail, user.ID)
		if err != nil {
			return errors.Annotate(err, "failed to check user email duplication")
		}
		if dup {
			reasons["mail"] = "mail duplicated"
		}
	}

	if len(reasons) > 0 {
		return service.NewValidationError(reasons)
	}
	return nil
}

func userExists(store repository.Store, id int) error {
	exists, err := store.User().Exists(id)
	if err != nil {
		return errors.Annotatef(err, "failed to check if user#%d exists", id)
	}
	if !exists {
		return errors.NotFoundf("user#%d", id)
	}
	return nil
}
