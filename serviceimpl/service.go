package serviceimpl

import (
	"codereview/repository"
	"codereview/service"
)

type Service struct {
	StoreFactory repository.StoreFactory
}

func reposSearchParam(sp *service.SearchParam) *repository.SearchParam {
	limit := sp.Limit
	if limit <= 0 {
		limit = -1
	}

	offset := sp.Offset
	if offset <= 0 {
		offset = -1
	}

	return &repository.SearchParam{
		Keyword: sp.Keyword,
		Limit:   limit,
		Offset:  offset,
	}
}
