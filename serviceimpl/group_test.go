package serviceimpl

import (
	"testing"

	"github.com/juju/errors"
	"github.com/stretchr/testify/assert"

	"codereview/model"
)

func TestGroupServiceGet(t *testing.T) {
	var (
		m        = &mockGroupRepos{}
		s        = &stubStore{group: m}
		expGroup = &model.Group{ID: 1, Name: "Group1", CreatedAt: now, UpdatedAt: now}
	)
	m.Test(t)
	m.On("Get", 1).Return(expGroup, nil)

	groupService := &GroupService{&Service{newStoreFactory(s)}}
	group, err := groupService.Get(ctx, 1)

	m.AssertExpectations(t)
	assert.True(t, s.storeClosed)
	assert.Equal(t, expGroup, group)
	assert.Nil(t, err)
}

func TestGroupServiceGet_notFound(t *testing.T) {
	var (
		m = &mockGroupRepos{}
		s = &stubStore{group: m}
	)
	m.Test(t)
	m.On("Get", 1).Return((*model.Group)(nil), errRecordNotFound)

	groupService := &GroupService{&Service{newStoreFactory(s)}}
	group, err := groupService.Get(ctx, 1)

	m.AssertExpectations(t)
	assert.True(t, s.storeClosed)
	assert.Nil(t, group)
	assert.True(t, errors.IsNotFound(err))
}

func TestGroupServiceGet_error(t *testing.T) {
	var (
		m = &mockGroupRepos{}
		s = &stubStore{group: m}
	)
	m.Test(t)
	m.On("Get", 1).Return((*model.Group)(nil), errOnRepos)

	groupService := &GroupService{&Service{newStoreFactory(s)}}
	group, err := groupService.Get(ctx, 1)

	m.AssertExpectations(t)
	assert.True(t, s.storeClosed)
	assert.Nil(t, group)
	assert.Equal(t, errOnRepos, errors.Cause(err))
}
