package serviceimpl

import (
	"reflect"
	"testing"
)

func TestDiffSlice(t *testing.T) {
	tests := []struct {
		a, b, want []int
	}{
		{[]int{1, 2}, []int{2, 3}, []int{1}},
		{[]int{1, 2}, []int{1, 2, 3}, []int{}},
		{[]int{1, 2}, []int{}, []int{1, 2}},
		{[]int{1, 1}, []int{2}, []int{1, 1}},
	}
	for _, tst := range tests {
		got := diffSlice(tst.a, tst.b)
		if !reflect.DeepEqual(got, tst.want) {
			t.Errorf("diffSlice(%v, %v): got %v, want %v", tst.a, tst.b, got, tst.want)
		}
	}
}

func TestUniqueSlice(t *testing.T) {
	tests := []struct {
		in, want []int
	}{
		{[]int{1, 1, 2, 2, 2}, []int{1, 2}},
		{[]int{3, 2, 1, 2, 1}, []int{3, 2, 1}},
	}
	for _, tst := range tests {
		got := uniqueSlice(tst.in)
		if !reflect.DeepEqual(got, tst.want) {
			t.Errorf("uniqueSlice(%v): got %v, want %v", tst.in, got, tst.want)
		}
	}
}
