package serviceimpl

import (
	"errors"
	"time"

	"github.com/stretchr/testify/mock"

	"codereview/model"
	"codereview/repository"
	"codereview/service"
)

var now = time.Now().Round(time.Second)
var ctx = &service.Context{}

var errRecordNotFound = errors.New("records not found")
var errOnRepos = errors.New("an error occured on repository")

type stubStore struct {
	group repository.GroupRepository
	user  repository.UserRepository

	storeClosed bool
}

func newStoreFactory(s repository.Store) func(repository.DBInfo) (repository.Store, error) {
	return func(repository.DBInfo) (repository.Store, error) {
		return s, nil
	}
}
func (s *stubStore) Group() repository.GroupRepository {
	return s.group
}
func (s *stubStore) User() repository.UserRepository {
	return s.user
}
func (s *stubStore) Close() error {
	s.storeClosed = true
	return nil
}
func (s *stubStore) Transact(txFunc func(repository.Store) error) error {
	return txFunc(s)
}
func (s *stubStore) IsRecordNotFoundError(err error) bool {
	return err == errRecordNotFound
}

type mockGroupRepos struct {
	mock.Mock
}

func (m *mockGroupRepos) Get(id int) (*model.Group, error) {
	args := m.Called(id)
	return args[0].(*model.Group), args.Error(1)
}
func (m *mockGroupRepos) Search(param *repository.SearchParam) (
	[]*model.Group, int, error) {
	args := m.Called(param)
	return args[0].([]*model.Group), args.Int(1), args.Error(2)
}
func (m *mockGroupRepos) Create(group *model.Group) error {
	args := m.Called(group)
	return args.Error(0)
}
func (m *mockGroupRepos) Update(group *model.Group) error {
	args := m.Called(group)
	return args.Error(0)
}
func (m *mockGroupRepos) Exists(id int) (bool, error) {
	args := m.Called(id)
	return args.Bool(0), args.Error(1)
}
func (m *mockGroupRepos) SearchUsers(id int, param *repository.SearchParam) (
	[]*model.User, int, error) {
	args := m.Called(id, param)
	return args[0].([]*model.User), args.Int(1), args.Error(2)
}
func (m *mockGroupRepos) AddUsers(groupID int, userIDs []int) error {
	args := m.Called(groupID, userIDs)
	return args.Error(0)
}
func (m *mockGroupRepos) RemoveUser(groupID, userID int) error {
	args := m.Called(groupID, userID)
	return args.Error(0)
}

type mockUserRepos struct {
	mock.Mock
}

func (m *mockUserRepos) Get(id int) (*model.User, error) {
	args := m.Called(id)
	return args[0].(*model.User), args.Error(1)
}
func (m *mockUserRepos) Search(param *repository.SearchParam) (
	[]*model.User, int, error) {
	args := m.Called(param)
	return args[0].([]*model.User), args.Int(1), args.Error(2)
}
func (m *mockUserRepos) Create(user *model.User) error {
	args := m.Called(user)
	return args.Error(0)
}
func (m *mockUserRepos) Update(user *model.User) error {
	args := m.Called(user)
	return args.Error(0)
}
func (m *mockUserRepos) Exists(id int) (bool, error) {
	args := m.Called(id)
	return args.Bool(0), args.Error(1)
}
func (m *mockUserRepos) FilterExists(userIDs []int) ([]int, error) {
	args := m.Called(userIDs)
	return args[0].([]int), args.Error(1)
}
func (m *mockUserRepos) DupMail(mail string, exid int) (bool, error) {
	args := m.Called(mail, exid)
	return args.Bool(0), args.Error(1)
}

type mockRepos struct {
	mock.Mock
}

func (m *mockRepos) Create(x interface{}) error {
	args := m.Called(x)
	return args.Error(0)
}
func (m *mockRepos) Update(x interface{}) error {
	args := m.Called(x)
	return args.Error(0)
}
