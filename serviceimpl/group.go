package serviceimpl

import (
	"strings"

	"github.com/juju/errors"

	"codereview/model"
	"codereview/repository"
	"codereview/service"
)

// GroupService GroupServiceの実装。
type GroupService struct {
	*Service
}

// Get グループを取得する。
func (s *GroupService) Get(c *service.Context, id int) (*model.Group, error) {
	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, err
	}
	defer store.Close()

	group, err := store.Group().Get(id)
	if err != nil {
		if store.IsRecordNotFoundError(err) {
			return nil, errors.NotFoundf("group#%d", id)
		}
		return nil, errors.Annotatef(err, "failed to get group#%d", id)
	}
	return group, nil
}

// Search グループを検索する。
func (s *GroupService) Search(c *service.Context, param *service.SearchParam) (
	list []*model.Group, total int, err error) {
	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, 0, err
	}
	defer store.Close()

	sp := reposSearchParam(param)
	list, total, err = store.Group().Search(sp)
	if err != nil {
		err = errors.Annotate(err, "failed to search groups")
	}
	return list, total, err
}

// Create グループを作成する。
func (s *GroupService) Create(c *service.Context, in *model.Group) (*model.Group, error) {
	group := &model.Group{
		Name: strings.TrimSpace(in.Name),
	}

	// 入力データを検証
	err := validateGroup(group)
	if err != nil {
		return nil, err
	}

	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, err
	}
	defer store.Close()

	// グループを作成
	err = store.Group().Create(group)
	if err != nil {
		return nil, errors.Annotate(err, "failed to create group")
	}
	return group, nil
}

// Update グループを更新する。
func (s *GroupService) Update(c *service.Context, in *model.Group) (*model.Group, error) {
	group := &model.Group{
		ID:   in.ID,
		Name: strings.TrimSpace(in.Name),
	}

	// 入力データを検証
	err := validateGroup(group)
	if err != nil {
		return nil, err
	}

	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, err
	}
	defer store.Close()

	// グループが存在するか確認
	err = groupExists(store, group.ID)
	if err != nil {
		return nil, err
	}

	// グループを更新
	err = store.Group().Update(group)
	if err != nil {
		return nil, errors.Annotate(err, "failed to update group")
	}
	return group, nil
}

func validateGroup(group *model.Group) error {
	reasons := map[string]string{}
	if group.Name == "" {
		reasons["name"] = "name required"
	}
	if len(reasons) > 0 {
		return service.NewValidationError(reasons)
	}
	return nil
}

func groupExists(store repository.Store, id int) error {
	exists, err := store.Group().Exists(id)
	if err != nil {
		return errors.Annotatef(err, "failed to check if group#%d exists", id)
	}
	if !exists {
		return errors.NotFoundf("group#%d", id)
	}
	return nil
}

// SearchUsers グループに所属するユーザを検索する。
func (s *GroupService) SearchUsers(c *service.Context, groupID int, param *service.SearchParam) (
	list []*model.User, total int, err error) {
	store, err := s.StoreFactory(c)
	if err != nil {
		return nil, 0, err
	}
	defer store.Close()

	sp := reposSearchParam(param)
	list, total, err = store.Group().SearchUsers(groupID, sp)
	if err != nil {
		err = errors.Annotate(err, "failed to search group users")
	}
	return list, total, err
}

// AddUsers グループにユーザを追加する。
func (s *GroupService) AddUsers(c *service.Context, groupID int, userIDs []int) error {
	store, err := s.StoreFactory(c)
	if err != nil {
		return err
	}
	defer store.Close()

	// グループが存在するか確認
	err = groupExists(store, groupID)
	if err != nil {
		return err
	}

	// ユーザが存在するか確認
	userIDs = uniqueSlice(userIDs)
	validUserIDs, err := store.User().FilterExists(userIDs)
	if err != nil {
		return errors.Annotatef(err, "failed to check if users exist")
	}
	if len(userIDs) != len(validUserIDs) {
		return errors.NotFoundf("user#%v", diffSlice(userIDs, validUserIDs))
	}

	err = store.Transact(func(store repository.Store) error {
		// グループにユーザを追加
		err := store.Group().AddUsers(groupID, userIDs)
		if err != nil {
			return errors.Annotatef(err, "failed to add users to group#%d", groupID)
		}
		return nil
	})
	return err
}

// RemoveUser グループからユーザを削除する。
func (s *GroupService) RemoveUser(c *service.Context, groupID, userID int) error {
	store, err := s.StoreFactory(c)
	if err != nil {
		return err
	}
	defer store.Close()

	// グループが存在するか確認
	err = groupExists(store, groupID)
	if err != nil {
		return err
	}

	// ユーザが存在するか確認
	err = userExists(store, userID)
	if err != nil {
		return err
	}

	// グループからユーザを削除
	err = store.Group().RemoveUser(groupID, userID)
	if err != nil {
		return errors.Annotatef(err, "failed to remove user#%d from group#%d", userID, groupID)
	}
	return nil
}
