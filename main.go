package main

import (
	"flag"
	"time"

	"github.com/gin-gonic/gin"

	"codereview/controller"
	"codereview/repositoryimpl"
	"codereview/serviceimpl"
)

var dsn string

func init() {
	cnf := loadMySQLConfig("./config_mysql.ini")
	cnf.Loc = time.Local
	cnf.ParseTime = true
	dsn = cnf.FormatDSN()
}

func main() {
	flag.Parse()
	if flag.Arg(0) == "initdb" {
		initDB()
		return
	}

	r := gin.Default()
	r.Use(func(c *gin.Context) {
		c.Set("DSN", dsn)
		c.Next()
	})

	s := &serviceimpl.Service{
		StoreFactory: repositoryimpl.New,
	}

	h := new(controller.Handler)
	h.Group.GroupService = &serviceimpl.GroupService{Service: s}
	h.User.UserService = &serviceimpl.UserService{Service: s}
	h.SetRoute(r.Group(""))

	r.Run()
}
