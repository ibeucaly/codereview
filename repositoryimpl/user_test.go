package repositoryimpl

import (
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"

	"codereview/model"
	repos "codereview/repository"
)

func TestUserGet(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	d := []*model.User{
		{ID: 1, Name: "Foo", Mail: "foo@example.com"},
	}
	setupData(t, store.db, InterfaceSlice(d))

	// test
	tests := []struct {
		groupID int
		err     error
		want    *model.User
	}{
		{1, nil, d[0]},                   // tet#0: 存在するレコード
		{2, gorm.ErrRecordNotFound, nil}, // test#1: 存在しないレコード
		{0, gorm.ErrRecordNotFound, nil}, // test#2: 存在しないレコード
	}

	for i, tst := range tests {
		got, err := store.User().Get(tst.groupID)
		assert.Equal(t, tst.want, got, "test#%d entity", i)
		assert.Equal(t, tst.err, err, "test#%d error", i)
	}
}

func TestUserSearch(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	d := []*model.User{
		{ID: 1, Name: "Foo", Mail: "foo@example.com"},
		{ID: 2, Name: "Bar", Mail: "b_a_r@example.com"},
		{ID: 3, Name: "FB", Mail: "foobar@example.com"},
	}
	setupData(t, store.db, InterfaceSlice(d))

	// tes
	tests := []struct {
		param     *repos.SearchParam
		want      []*model.User
		wantTotal int
	}{
		// test#0: 全取得
		{defaultSearchParam, []*model.User{d[0], d[1], d[2]}, 3},
		// test#1: 検索
		{searchParam("ba", 2, 1), []*model.User{d[2]}, 2},
	}

	for i, tst := range tests {
		list, total, err := store.User().Search(tst.param)
		if !assert.NoError(t, err, "test#%d error", i) {
			continue
		}
		assert.Equal(t, tst.want, list, "test#%d list", i)
		assert.Equal(t, tst.wantTotal, total, "test#%d total", i)
	}
}

func TestUserExists(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	setupData(t, store.db, []interface{}{
		&model.User{ID: 1},
	})

	// test
	tests := []struct {
		id   int
		want bool
	}{
		{1, true},  // test#0: 存在するレコード
		{2, false}, // test#1: 存在しないレコード
		{0, false}, // test#2: 存在しないレコード
	}

	for i, tst := range tests {
		got, err := store.User().Exists(tst.id)
		assert.Equal(t, tst.want, got, "test#%d result", i)
		assert.NoError(t, err, "test#%d error", i)
	}
}

func TestUserDupMail(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	setupData(t, store.db, []interface{}{
		&model.User{ID: 1, Mail: "foo@example.com"},
		&model.User{ID: 2, Mail: "bar@example.com"},
	})

	// test
	tests := []struct {
		mail       string
		excludedID int
		want       bool
	}{

		{"foo@example.com", 0, true},   // test#0: 重複
		{"foo@example.com", 1, false},  // test#1: 除外対象
		{"bar2@example.com", 0, false}, // test#2: ユニーク
	}

	for i, tst := range tests {
		got, err := store.User().DupMail(tst.mail, tst.excludedID)
		assert.Equal(t, tst.want, got, "test#%d result", i)
		assert.NoError(t, err, "test#%d error", i)
	}
}
func TestUserFilterExists(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	setupData(t, store.db, []interface{}{
		&model.User{ID: 1},
		&model.User{ID: 3},
	})

	// test
	tests := []struct {
		ids  []int
		want []int
	}{
		{[]int{0, 1, 2, 3, 3}, []int{1, 3}}, // test#0: 存在するIDあり
		{[]int{2, 4, 4}, nil},               // test#1: 存在しないIDのみ
	}

	for i, tst := range tests {
		got, err := store.User().FilterExists(tst.ids)
		assert.Equal(t, tst.want, got, "test#%d result", i)
		assert.NoError(t, err, "test#%d error", i)
	}
}
