package repositoryimpl

import (
	"testing"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"

	"codereview/model"
	repos "codereview/repository"
)

func TestGroupGet(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	d := []*model.Group{
		{ID: 1, Name: "Foo"},
	}
	setupData(t, store.db, InterfaceSlice(d))

	// test
	tests := []struct {
		groupID int
		err     error
		want    *model.Group
	}{
		{1, nil, d[0]},                   // test#0: 存在するレコード
		{2, gorm.ErrRecordNotFound, nil}, // test#1: 存在しないレコード
		{0, gorm.ErrRecordNotFound, nil}, // test#2: 存在しないレコード
	}

	for i, tst := range tests {
		got, err := store.Group().Get(tst.groupID)
		assert.Equal(t, tst.want, got, "test#%d Group", i)
		assert.Equal(t, tst.err, err, "test#%d error", i)
	}
}

func TestGroupSearch(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	d := []*model.Group{
		&model.Group{ID: 1, Name: "Foo"},
		&model.Group{ID: 2, Name: "Bar"},
		&model.Group{ID: 3, Name: "Baz"},
	}
	setupData(t, store.db, InterfaceSlice(d))

	// test
	tests := []struct {
		param     *repos.SearchParam
		want      []*model.Group
		wantTotal int
	}{
		// test#0: 全取得
		{defaultSearchParam, []*model.Group{d[0], d[1], d[2]}, 3},
		// test#1: 検索
		{searchParam("ba", 2, 1), []*model.Group{d[2]}, 2},
	}

	for i, tst := range tests {
		list, total, err := store.Group().Search(tst.param)
		if !assert.NoError(t, err, "test#%d error", i) {
			continue
		}
		assert.Equal(t, tst.want, list, "test#%d list", i)
		assert.Equal(t, tst.wantTotal, total, "test#%d total", i)
	}
}

func TestGroupSearchUsers(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	d := []*model.User{
		&model.User{ID: 101, Name: "Foo"},
		&model.User{ID: 102, Name: "Bar"},
		&model.User{ID: 103, Name: "Baz"},
		&model.User{ID: 104, Name: "FooBar"},
	}
	setupData(t, store.db, InterfaceSlice(d))
	setupData(t, store.db, []interface{}{
		&model.Group{ID: 1},
		&model.GroupUser{GroupID: 1, UserID: 101},
		&model.GroupUser{GroupID: 1, UserID: 102},
		&model.GroupUser{GroupID: 1, UserID: 103},
	})

	// test
	tests := []struct {
		groupID   int
		param     *repos.SearchParam
		want      []*model.User
		wantTotal int
	}{
		// test#0: 全取得
		{1, defaultSearchParam, []*model.User{d[0], d[1], d[2]}, 3},
		// test#1: 検索
		{1, searchParam("ba", 2, 1), []*model.User{d[2]}, 2},
	}

	for i, tst := range tests {
		list, total, err := store.Group().SearchUsers(tst.groupID, tst.param)
		if !assert.NoError(t, err, "test#%d error", i) {
			continue
		}
		assert.Equal(t, tst.want, list, "test#%d list", i)
		assert.Equal(t, tst.wantTotal, total, "test#%d total", i)
	}
}

func TestGroupAddUsers(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	ago := now.Add(-time.Hour)

	setupData(t, store.db, []interface{}{
		&model.GroupUser{GroupID: 1, UserID: 1, CreatedAt: ago, UpdatedAt: ago},
		&model.GroupUser{GroupID: 2, UserID: 2, CreatedAt: ago, UpdatedAt: ago},
	})

	// tests
	tests := []struct {
		groupID int
		userIDs []int
		err     error
		want    []*model.GroupUser
	}{
		// test#0: OK
		{1, []int{1, 2, 3}, nil, []*model.GroupUser{
			{GroupID: 1, UserID: 1, CreatedAt: ago, UpdatedAt: ago},
			{GroupID: 1, UserID: 2, CreatedAt: now, UpdatedAt: now},
			{GroupID: 1, UserID: 3, CreatedAt: now, UpdatedAt: now},
		}},
		// test#1: 不正なユーザID
		{4, []int{1, 0}, ErrBlankPrimaryKey, nil},
		// test#2: 不正なグループID
		{0, []int{1, 2}, ErrBlankPrimaryKey, nil},
	}

	for i, tst := range tests {
		err := store.Group().AddUsers(tst.groupID, tst.userIDs)
		if !assert.Equal(t, tst.err, err, "test#%d error", i) {
			continue
		}
		for _, w := range tst.want {
			g := &model.GroupUser{}
			err := store.db.Where("GROUP_id = ? AND USER_id = ?", w.GroupID, w.UserID).First(g).Error
			if assert.NoError(t, err, "test#%d db error", i) {
				assert.Equal(t, w, g, "test#%d record", i)
			}
		}
	}
}

func TestGroupRemoveUser(t *testing.T) {
	store, teardown := setup(t)
	defer teardown()

	setupData(t, store.db, []interface{}{
		&model.GroupUser{GroupID: 1, UserID: 2},
	})

	// test
	tests := []struct {
		groupID int
		userID  int
		err     error
	}{
		{1, 2, nil},                // test#0: 存在するレコード
		{1, 1, nil},                // test#1: 存在しないレコード
		{0, 1, ErrBlankPrimaryKey}, // test#2: 不正なグループID
		{1, 0, ErrBlankPrimaryKey}, // test#3: 不正なユーザID
	}

	for i, tst := range tests {
		err := store.Group().RemoveUser(tst.groupID, tst.userID)
		if !assert.Equal(t, tst.err, err, "test#%d error", i) {
			continue
		}
		if tst.err == nil {
			// 削除されているか確認
			notFound := store.db.
				First(&model.GroupUser{}, "GROUP_id = ? AND USER_id = ?", tst.groupID, tst.userID).
				RecordNotFound()
			assert.True(t, notFound, "test#%d record found", i)
		}
	}
}
