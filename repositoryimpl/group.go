package repositoryimpl

import (
	"sort"

	"codereview/model"
	repos "codereview/repository"
)

// GroupRepository グループリポジトリ
type GroupRepository repository

// Get 指定されたIDのグループレコードを取得する。
func (r *GroupRepository) Get(id int) (*model.Group, error) {
	group := &model.Group{}
	err := r.store.db.First(group, id).Error
	if err != nil {
		return nil, err
	}
	return group, nil
}

// Search グループレコードを検索する。
func (r *GroupRepository) Search(param *repos.SearchParam) ([]*model.Group, int, error) {
	var groups []*model.Group
	var count int

	db := r.store.db.Model(&model.Group{})
	if param.Keyword != "" {
		q := "%" + param.Keyword + "%"
		db = db.Where("name LIKE ?", q)
	}
	db = db.
		Count(&count).
		Limit(param.Limit).
		Offset(param.Offset).
		Find(&groups)

	if err := db.Error; err != nil {
		return nil, 0, err
	}
	return groups, count, nil
}

// Create グループレコードを作成する。
func (r *GroupRepository) Create(group *model.Group) error {
	return r.store.create(group)
}

// Update グループレコードを更新する。
// 更新対象列: Name
func (r *GroupRepository) Update(group *model.Group) error {
	return r.store.update(
		group,
		map[string]interface{}{
			"name": group.Name,
		})
}

// Exists 指定されたIDのグループが存在すれば真を返す。
func (r *GroupRepository) Exists(id int) (bool, error) {
	return r.store.existsScope(
		&model.Group{},
		scopeWhere("id = ?", id))
}

// SearchUsers 指定されたグループに所属するユーザを検索する。
func (r *GroupRepository) SearchUsers(id int, param *repos.SearchParam) ([]*model.User, int, error) {
	var users []*model.User
	var count int

	db := r.store.db.
		Model(&model.User{}).
		Select("USER.*").
		Joins("JOIN GROUP_USER ON USER.id = GROUP_USER.USER_id").
		Where("GROUP_USER.GROUP_id = ?", id)

	if param.Keyword != "" {
		q := "%" + param.Keyword + "%"
		db = db.Where("USER.name LIKE ? OR USER.mail LIKE ?", q, q)
	}

	db = db.
		Count(&count).
		Limit(param.Limit).
		Offset(param.Offset).
		Find(&users)

	if err := db.Error; err != nil {
		return nil, 0, err
	}
	return users, count, nil
}

// AddUsers グループとユーザの紐付けレコードを複数作成する。
// 与えられたユーザIDの昇順に1件ずつ作成し、エラーが起きたらそこで終了する。
// すでに紐付けレコードが存在する場合、そのレコードに対しては何もしない。
func (r *GroupRepository) AddUsers(groupID int, userIDs []int) error {
	relation := &model.GroupUser{
		GroupID: groupID,
	}

	sort.Ints(userIDs)

	for _, userID := range userIDs {
		relation.UserID = userID
		exists, err := r.store.exists(relation)
		if err == nil && !exists {
			err = r.store.create(relation)
		}
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveUser グループとユーザの紐付けレコードを削除する。
// 紐付けレコードが存在しなくても、エラーにしない。
func (r *GroupRepository) RemoveUser(id, userID int) error {
	rel := &model.GroupUser{
		GroupID: id,
		UserID:  userID,
	}
	return r.store.delete(rel)
}
