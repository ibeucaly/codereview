package repositoryimpl

import (
	"errors"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" // needed for gorm.Open

	repos "codereview/repository"
)

type Store struct {
	db     *gorm.DB
	common repository
}

type repository struct {
	store *Store
}

// New 新しいStoreを作成して返す。
func New(info repos.DBInfo) (repos.Store, error) {
	db, err := gorm.Open("mysql", info.DSN())
	if err != nil {
		return nil, err
	}
	db.LogMode(info.LogEnabled())

	s := &Store{db: db}
	s.common.store = s
	return s, nil
}

func (s *Store) Transact(txFunc func(repos.Store) error) error {
	var err error

	db := s.db.Begin()
	defer func() {
		if p := recover(); p != nil {
			db.Rollback()
			panic(p)
		} else if err != nil {
			db.Rollback()
		} else {
			db.Commit()
		}
	}()

	txStore := &Store{db: db}
	err = txFunc(txStore)
	return err
}

func (s *Store) Close() error {
	return s.db.Close()
}

func (s *Store) Group() repos.GroupRepository {
	return (*GroupRepository)(&s.common)
}

func (s *Store) User() repos.UserRepository {
	return (*UserRepository)(&s.common)
}

func (s *Store) IsRecordNotFoundError(err error) bool {
	return gorm.IsRecordNotFoundError(err)
}

// ErrNoRecordsUpdated 更新されたレコードがなかったときに返すエラー。
var ErrNoRecordsUpdated = errors.New("no records updated")

// ErrBlankPrimaryKey 主キーが指定されなかったときに返すエラー。
var ErrBlankPrimaryKey = errors.New("blank primary key")

// create 新規レコードを作成する。
func (s *Store) create(v interface{}) error {
	return s.db.Create(v).Error
}

// update 既存レコードの指定された列を更新する。
// 主キー指定必須。更新されるレコードがない場合、エラーを返す。
func (s *Store) update(
	model interface{},
	values map[string]interface{},
	scopes ...func(*gorm.DB) *gorm.DB,
) error {
	if s.primaryKeysZero(model) {
		return ErrBlankPrimaryKey
	}

	db := s.db.Model(model).Scopes(scopes...).Updates(values)
	if err := db.Error; err != nil {
		return err
	}
	if db.RowsAffected == 0 {
		return ErrNoRecordsUpdated
	}
	return nil
}

// delete 既存レコードを削除する。
// 主キー指定必須。
func (s *Store) delete(value interface{}) error {
	if s.primaryKeysZero(value) {
		return ErrBlankPrimaryKey
	}
	return s.db.Delete(value).Error
}

// exists 指定された主キーを持つレコードが存在すれば真を返す。
// 主キー指定必須。
func (s *Store) exists(value interface{}) (bool, error) {
	if s.primaryKeysZero(value) {
		return false, ErrBlankPrimaryKey
	}

	var count int
	err := s.db.Model(value).Limit(1).Count(&count).Error
	return err == nil && count > 0, err
}

// existsScope 指定された条件のレコードが存在すれば真を返す。
// NOTE: modelの主キーが非ゼロの場合、それも条件に加えられる。
func (s *Store) existsScope(
	model interface{},
	scope func(*gorm.DB) *gorm.DB,
) (bool, error) {
	var count int
	err := s.db.Model(model).Scopes(scope).Limit(1).Count(&count).Error
	return err == nil && count > 0, err
}

// primaryKeysZero 主キーがゼロならば真を返す。
// gormのPrimaryKeyZeroが複合主キーに対応していないので、自前で用意。。
func (s *Store) primaryKeysZero(value interface{}) bool {
	fields := s.db.NewScope(value).PrimaryFields()
	for _, f := range fields {
		if f.IsBlank {
			return true
		}
	}
	return false
}

// scopeFunc Where句を付与する関数。
type scopeFunc func(*gorm.DB) *gorm.DB

// scopeWhere 指定された条件のWHERE句を付与する関数を返す。
func scopeWhere(query interface{}, args ...interface{}) scopeFunc {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where(query, args...)
	}
}
