package repositoryimpl

import (
	"net"
	"reflect"
	"testing"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"

	"codereview/model"
	repos "codereview/repository"
)

var (
	dsn       string
	available bool
	now       time.Time
)

func init() {
	cnf := mysql.Config{
		User:      "cruser",
		Passwd:    "",
		Net:       "tcp",
		Addr:      "localhost:3306",
		DBName:    "codereviewtest",
		Collation: "utf8_general_ci",
		ParseTime: true,
		Loc:       time.Local,
	}
	dsn = cnf.FormatDSN()
	c, err := net.Dial(cnf.Net, cnf.Addr)
	if err == nil {
		available = true
		c.Close()
	}
	now = time.Now().Round(time.Second)
}

// setup DBに接続する。接続に失敗したら、テストを終了させる。
// Store、gorm.DBへの参照と、DB接続を切断する関数を返す。
func setup(t *testing.T) (store *Store, teardown func() error) {
	if !available {
		t.Fatalf("MySQL server not running")
	}
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		t.Fatal("connection error:", err)
	}

	db.Callback().Create().Replace("gorm:update_time_stamp", func(scope *gorm.Scope) {
		if !scope.HasError() {
			if createdAtField, ok := scope.FieldByName("CreatedAt"); ok {
				if createdAtField.IsBlank {
					createdAtField.Set(now)
				}
			}
			if updatedAtField, ok := scope.FieldByName("UpdatedAt"); ok {
				if updatedAtField.IsBlank {
					updatedAtField.Set(now)
				}
			}
		}
	})
	db.Callback().Update().Replace("gorm:update_time_stamp", func(scope *gorm.Scope) {
		if _, ok := scope.Get("gorm:update_column"); !ok {
			scope.SetColumn("UpdatedAt", now)
		}
	})

	store = &Store{db: db}
	store.common.store = store
	return store, store.Close

}

// setupData 渡されたデータについて、テーブルを新しく作成し、データを挿入する。
// 失敗したら、テストを終了させる。
func setupData(t *testing.T, db *gorm.DB, values []interface{}) {
	// テーブルリストを作成
	tables := make([]interface{}, 0, len(values))
	types := make(map[reflect.Type]bool)
	for _, v := range values {
		t := reflect.TypeOf(v)
		if t.Kind() == reflect.Struct {
			t = reflect.PtrTo(t)
		}
		if !types[t] {
			tables = append(tables, v)
			types[t] = true
		}
	}

	// DROP TABLE, CREATE TABLE
	if err := createTables(db, tables); err != nil {
		t.Fatal("table setup error:", err)
	}

	// INSERT
	for _, v := range values {
		db = db.Create(v)
	}
	if db.Error != nil {
		t.Fatal("data insert error:", db.Error)
	}
}

func createTables(db *gorm.DB, values []interface{}) error {
	if err := db.DropTableIfExists(values...).Error; err != nil {
		return err
	}
	if err := db.AutoMigrate(values...).Error; err != nil {
		return err
	}
	return nil
}

func InterfaceSlice(s interface{}) []interface{} {
	if reflect.TypeOf(s).Kind() != reflect.Slice {
		panic("must be a slice")
	}

	v := reflect.ValueOf(s)
	len := v.Len()
	t := make([]interface{}, len)

	for i := 0; i < len; i++ {
		t[i] = v.Index(i).Interface()
	}
	return t
}

func TestInterfaceSlice(t *testing.T) {
	assert.Equal(t, []interface{}{1, 2, 3}, InterfaceSlice([]int{1, 2, 3}))
	assert.Equal(t, []interface{}{}, InterfaceSlice([]string(nil)))
	assert.Equal(t, []interface{}{&model.Group{ID: 1}}, InterfaceSlice([]*model.Group{{ID: 1}}))
	assert.Panics(t, func() { InterfaceSlice(nil) })
	assert.Panics(t, func() { InterfaceSlice([1]int{1}) })
}

var defaultSearchParam = &repos.SearchParam{
	Limit:  -1,
	Offset: -1,
}

func searchParam(keyword string, limit, offset int) *repos.SearchParam {
	return &repos.SearchParam{
		Keyword: keyword,
		Limit:   limit,
		Offset:  offset,
	}
}

type dbinfo struct {
	dsn string
	log bool
}

func (d dbinfo) DSN() string      { return d.dsn }
func (d dbinfo) LogEnabled() bool { return d.log }

func TestNew(t *testing.T) {
	if !available {
		t.Fatalf("MySQL server not running")
	}

	var store repos.Store
	var err error

	// valid dsn
	store, err = New(&dbinfo{dsn: dsn})
	assert.NotNil(t, store)
	assert.IsType(t, &Store{}, store)
	assert.Nil(t, err)

	// invalid dsn
	store, err = New(&dbinfo{dsn: ""})
	assert.Nil(t, store)
	assert.Error(t, err)
}
