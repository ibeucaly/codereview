package repositoryimpl

import (
	"codereview/model"
	repos "codereview/repository"
)

// UserRepository ユーザリポジトリ
type UserRepository repository

// Get 指定されたIDのユーザレコードを取得する。
func (r *UserRepository) Get(id int) (*model.User, error) {
	user := &model.User{}
	err := r.store.db.First(user, id).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

// Search ユーザレコードを検索する。
func (r *UserRepository) Search(param *repos.SearchParam) ([]*model.User, int, error) {
	var users []*model.User
	var count int

	db := r.store.db.Model(&model.User{})
	if param.Keyword != "" {
		q := "%" + param.Keyword + "%"
		db = db.Where("name LIKE ? OR mail LIKE ?", q, q)
	}
	db = db.
		Count(&count).
		Limit(param.Limit).
		Offset(param.Offset).
		Find(&users)

	if err := db.Error; err != nil {
		return nil, 0, err
	}
	return users, count, nil
}

// Create ユーザレコードを作成する。
func (r *UserRepository) Create(user *model.User) error {
	return r.store.create(user)
}

// Update ユーザレコードを更新する。
// 更新対象列: Name, Mail
func (r *UserRepository) Update(user *model.User) error {
	return r.store.update(
		user,
		map[string]interface{}{
			"name": user.Name,
			"mail": user.Mail,
		})
}

// Exists 指定されたIDのユーザが存在すれば真を返す。
func (r *UserRepository) Exists(id int) (bool, error) {
	return r.store.existsScope(
		&model.User{},
		scopeWhere("id = ?", id))
}

// DupMail 指定されたEmailがすでに使用されていれば真を返す。
func (r *UserRepository) DupMail(mail string, excludedID int) (bool, error) {
	var cond scopeFunc
	if excludedID != 0 {
		cond = scopeWhere("mail = ? AND id <> ?", mail, excludedID)
	} else {
		cond = scopeWhere("mail = ?", mail)
	}
	return r.store.existsScope(&model.User{}, cond)
}

// FilterExists 与えられたIDリストから、存在するユーザIDを抽出する。
func (r *UserRepository) FilterExists(ids []int) ([]int, error) {
	var validIDs []int

	err := r.store.db.Model(&model.User{}).
		Where("id IN (?)", ids).
		Pluck("id", &validIDs).
		Error

	if err != nil {
		return nil, err
	}
	return validIDs, nil
}
