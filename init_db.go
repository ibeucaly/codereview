package main

import (
	"codereview/model"
	"log"

	"github.com/jinzhu/gorm"
)

func initDB() {
	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	tables := []interface{}{
		&model.User{},
		&model.Group{},
		&model.GroupUser{},
	}
	for _, v := range tables {
		if err := db.DropTableIfExists(v).CreateTable(v).Error; err != nil {
			panic(err)
		}
	}

	users := []*model.User{
		{ID: 101, Name: "Foo", Mail: "foo@example.com"},
		{ID: 102, Name: "Bar", Mail: "bar@example.com"},
		{ID: 103, Name: "Baz", Mail: "baz@example.com"},
		{ID: 104, Name: "Qux", Mail: "qux@xyz.co.jp"},
		{ID: 105, Name: "Quux", Mail: "quux@xyz.co.jp"},
		{ID: 106, Name: "Corge", Mail: "corge@xyz.co.jp"},
		{ID: 107, Name: "Grault", Mail: "grault@testgroup.net"},
		{ID: 108, Name: "Garply", Mail: "garply@testgroup.net"},
		{ID: 109, Name: "Waldo", Mail: "waldo@example2.com"},
		{ID: 110, Name: "Fred", Mail: "fred@example2.com"},
	}
	groups := []*model.Group{
		{ID: 1, Name: "example"},
		{ID: 2, Name: "xyz"},
		{ID: 3, Name: "test group"},
		{ID: 4, Name: "every"},
	}
	groupusers := []*model.GroupUser{
		{GroupID: 1, UserID: 101},
		{GroupID: 1, UserID: 102},
		{GroupID: 1, UserID: 103},
		{GroupID: 2, UserID: 104},
		{GroupID: 2, UserID: 105},
		{GroupID: 2, UserID: 106},
		{GroupID: 3, UserID: 107},
		{GroupID: 3, UserID: 108},
		{GroupID: 4, UserID: 101},
		{GroupID: 4, UserID: 102},
		{GroupID: 4, UserID: 103},
		{GroupID: 4, UserID: 104},
		{GroupID: 4, UserID: 105},
		{GroupID: 4, UserID: 106},
		{GroupID: 4, UserID: 107},
		{GroupID: 4, UserID: 108},
		{GroupID: 4, UserID: 109},
		{GroupID: 4, UserID: 110},
	}

	for _, v := range users {
		db = db.Create(v)
	}
	for _, v := range groups {
		db = db.Create(v)
	}
	for _, v := range groupusers {
		db = db.Create(v)
	}
	if db.Error != nil {
		log.Printf("Error: %v", db.Error)
	}
}
