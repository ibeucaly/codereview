package service

import (
	"strings"

	"github.com/juju/errors"
)

// ValidationError 検証エラー。
type ValidationError struct {
	errors.Err
	Reasons map[string]string
}

func (verr *ValidationError) Message() string {
	reason := make([]string, len(verr.Reasons))
	i := 0
	for _, v := range verr.Reasons {
		reason[i] = v
		i++
	}
	return strings.Join(reason, ", ")
}

func NewValidationError(reasons map[string]string) *ValidationError {
	err := &ValidationError{
		errors.NewErr("validation error"),
		reasons,
	}
	err.SetLocation(1)
	return err
}

func IsValidationError(err error) bool {
	err = errors.Cause(err)
	_, ok := err.(*ValidationError)
	return ok
}

func ValidationErrorReasons(err error) map[string]string {
	err = errors.Cause(err)
	verr, ok := err.(*ValidationError)
	if !ok {
		return nil
	}
	return verr.Reasons
}

func ValidationErrorMessage(err error) string {
	err = errors.Cause(err)
	verr, ok := err.(*ValidationError)
	if !ok {
		return ""
	}
	return verr.Message()
}
