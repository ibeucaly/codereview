package service

import (
	"codereview/model"
)

// GroupService グループ関連のサービスインタフェース。
type GroupService interface {
	Get(*Context, int) (*model.Group, error)
	Search(*Context, *SearchParam) ([]*model.Group, int, error)
	Create(*Context, *model.Group) (*model.Group, error)
	Update(*Context, *model.Group) (*model.Group, error)

	SearchUsers(c *Context, groupID int, sp *SearchParam) ([]*model.User, int, error)
	AddUsers(c *Context, groupID int, userIDs []int) error
	RemoveUser(c *Context, groupID, userID int) error
}
