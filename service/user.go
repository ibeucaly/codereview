package service

import (
	"codereview/model"
)

// UserService ユーザ関連のサービス。
type UserService interface {
	Get(*Context, int) (*model.User, error)
	Search(*Context, *SearchParam) ([]*model.User, int, error)
	Create(*Context, *model.User) (*model.User, error)
	Update(*Context, *model.User) (*model.User, error)
}
