package service

type Context struct {
	SiteID  string
	UserID  int
	GroupID int

	DBDSN        string
	DBLogEnabled bool
}

func (c *Context) DSN() string {
	return c.DBDSN
}
func (c *Context) LogEnabled() bool {
	return c.DBLogEnabled
}
