package main

import (
	"log"

	"github.com/BurntSushi/toml"
	"github.com/go-sql-driver/mysql"
)

func loadMySQLConfig(inipath string) *mysql.Config {
	var config mysql.Config
	if _, err := toml.DecodeFile(inipath, &config); err != nil {
		log.Fatalf("Failed to decode %s: %v", inipath, err)
	}
	return &config
}
