package repository

import "codereview/model"

// UserRepository Userレコード関連の操作をするインタフェース。
type UserRepository interface {
	Get(int) (*model.User, error)
	Search(*SearchParam) ([]*model.User, int, error)
	Create(*model.User) error
	Update(*model.User) error
	Exists(int) (bool, error)
	FilterExists([]int) ([]int, error)
	DupMail(string, int) (bool, error)
}
