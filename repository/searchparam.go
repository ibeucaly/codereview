package repository

// SearchParam 検索パラメタ
type SearchParam struct {
	Keyword string
	Limit   int
	Offset  int
}
