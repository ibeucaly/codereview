package repository

// Store 各リポジトリを統括するインタフェース。
type Store interface {
	Transact(func(Store) error) error
	Close() error

	Group() GroupRepository
	User() UserRepository

	IsRecordNotFoundError(error) bool
}

type StoreFactory func(DBInfo) (Store, error)

type DBInfo interface {
	DSN() string
	LogEnabled() bool
}
