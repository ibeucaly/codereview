package repository

import "codereview/model"

// GroupRepository Groupレコード関連の操作をするインタフェース。
type GroupRepository interface {
	Get(int) (*model.Group, error)
	Search(*SearchParam) ([]*model.Group, int, error)
	Create(*model.Group) error
	Update(*model.Group) error
	Exists(int) (bool, error)
	SearchUsers(int, *SearchParam) ([]*model.User, int, error)
	AddUsers(int, []int) error
	RemoveUser(int, int) error
}
