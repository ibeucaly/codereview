package model

import "time"

type User struct {
	ID        int       `gorm:"primary_key" json:"id"`                            // ユーザID
	Name      string    `gorm:"type:varchar(255);size:255" json:"name"`           // 氏名
	Mail      string    `gorm:"type:varchar(255);size:255" json:"mail"`           // メールアドレス
	CreatedAt time.Time `gorm:"type:datetime;column:created_at" json:"createdAt"` // 作成日時
	UpdatedAt time.Time `gorm:"type:datetime;column:updated_at" json:"updatedAt"` // 更新日時
}

type Group struct {
	ID        int       `gorm:"primary_key;" json:"id"`                           // グループID
	Name      string    `gorm:"type:varchar(200);size:200" json:"name"`           // グループ名称
	CreatedAt time.Time `gorm:"type:datetime;column:created_at" json:"createdAt"` // 作成日時
	UpdatedAt time.Time `gorm:"type:datetime;column:updated_at" json:"updatedAt"` // 更新日時
}

type GroupUser struct {
	GroupID   int       `gorm:"type:int(11);primary_key;column:GROUP_id"`         // グループID
	UserID    int       `gorm:"type:int(11);primary_key;column:USER_id"`          // ユーザID
	CreatedAt time.Time `gorm:"type:datetime;column:created_at" json:"createdAt"` // 作成日時
	UpdatedAt time.Time `gorm:"type:datetime;column:updated_at"`                  // 更新日時
}

func (User) TableName() string      { return "USER" }
func (Group) TableName() string     { return "GROUP" }
func (GroupUser) TableName() string { return "GROUP_USER" }
