package controller

import (
	"github.com/gin-gonic/gin"

	"codereview/model"
	"codereview/service"
)

type UserHandler struct {
	UserService service.UserService
}

// List ユーザリストを取得する。
func (h *UserHandler) List(c *gin.Context) {
	searchParam, err := getQuerySearchParam(c)
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	users, total, err := h.UserService.Search(sc, searchParam)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{
		"data":  users,
		"total": total,
	})
}

// GetByID 指定されたIDのユーザを取得する。
func (h *UserHandler) GetByID(c *gin.Context) {
	id, err := getURLParamInt(c, "id")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	user, err := h.UserService.Get(sc, id)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{"data": user})
}

// Create ユーザを作成する。
func (h *UserHandler) Create(c *gin.Context) {
	user := &model.User{}
	err := c.ShouldBindJSON(user)
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	user, err = h.UserService.Create(sc, user)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{"data": user})
}

// Update ユーザを更新する。
func (h *UserHandler) Update(c *gin.Context) {
	id, err := getURLParamInt(c, "id")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	user := &model.User{}
	err = c.ShouldBindJSON(user)
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	user.ID = id
	user, err = h.UserService.Update(sc, user)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{"data": user})
}
