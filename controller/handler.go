package controller

import (
	"github.com/gin-gonic/gin"
)

// Handler リクエストハンドラ。
type Handler struct {
	Group GroupHandler
	User  UserHandler
}

// SetRoute
func (h Handler) SetRoute(r *gin.RouterGroup) {
	r.GET("/users", h.User.List)
	r.GET("/users/:id", h.User.GetByID)
	r.POST("/users", h.User.Create)
	r.PUT("/users/:id", h.User.Update)

	r.GET("/groups", h.Group.List)
	r.GET("/groups/:id", h.Group.GetByID)
	r.POST("/groups", h.Group.Create)
	r.PUT("/groups/:id", h.Group.Update)

	r.GET("/groups/:id/users", h.Group.UserList)
	r.POST("/groups/:id/users", h.Group.UserAdd)
	r.DELETE("/groups/:id/users/:userID", h.Group.UserRemove)
}
