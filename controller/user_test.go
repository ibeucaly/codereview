package controller

import (
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"codereview/model"
	"codereview/service"
)

func TestUserHandlerGetByID(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Params = []gin.Param{{Key: "id", Value: "1"}}

	m := &mockUserService{}
	m.On("Get", &dmysc, 1).Return(
		&model.User{
			ID:        1,
			Name:      "User1",
			Mail:      "user1@test.com",
			CreatedAt: now,
			UpdatedAt: now,
		}, nil)

	h := UserHandler{UserService: m}
	h.GetByID(c)

	m.AssertExpectations(t)
	exp := fmt.Sprintf(`{
		"data": {"id": 1, "name": "User1", "mail": "user1@test.com", "createdAt": %s, "updatedAt": %s}
	}`, nowStr, nowStr)
	assert.JSONEq(t, exp, rw.Body.String())
}

type mockUserService struct {
	mock.Mock
}

func (m *mockUserService) Get(c *service.Context, id int) (*model.User, error) {
	args := m.Called(c, id)
	return args[0].(*model.User), args.Error(1)
}
func (m *mockUserService) Search(c *service.Context, p *service.SearchParam) ([]*model.User, int, error) {
	args := m.Called(c, p)
	return args[0].([]*model.User), args.Int(1), args.Error(2)
}
func (m *mockUserService) Create(c *service.Context, in *model.User) (*model.User, error) {
	args := m.Called(c, in)
	return args[0].(*model.User), args.Error(1)
}
func (m *mockUserService) Update(c *service.Context, in *model.User) (*model.User, error) {
	args := m.Called(c, in)
	return args[0].(*model.User), args.Error(1)
}
