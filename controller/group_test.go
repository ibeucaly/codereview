package controller

import (
	"bytes"
	"fmt"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"codereview/model"
	"codereview/service"
)

func TestGroupHandlerList(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Request = httptest.NewRequest("GET", "/?keyword=abc&limit=1&offset=2", nil)

	m := &mockGroupService{}
	m.On("Search", &dmysc, &service.SearchParam{
		Keyword: "abc",
		Limit:   1,
		Offset:  2,
	}).Return([]*model.Group{{ID: 1, Name: "Group1", CreatedAt: now, UpdatedAt: now}}, 1, nil)

	h := GroupHandler{GroupService: m}
	h.List(c)

	m.AssertExpectations(t)
	exp := fmt.Sprintf(`{
		"data": [{"id": 1, "name": "Group1", "createdAt": %s, "updatedAt": %s}],
		"total": 1
	}`, nowStr, nowStr)
	assert.JSONEq(t, exp, rw.Body.String())
}

func TestGroupHandlerList_error(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Request = httptest.NewRequest("GET", "/", nil)

	m := &mockGroupService{}
	m.On("Search", &dmysc, mock.Anything).
		Return(([]*model.Group)(nil), 0, errors.New("an error"))

	h := GroupHandler{GroupService: m}
	h.List(c)

	m.AssertExpectations(t)
	assert.Equal(t, 400, rw.Code)
	assert.JSONEq(t, `{"message": "an error"}`, rw.Body.String())
}

func TestGroupHandlerList_invQuery(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Request = httptest.NewRequest("GET", "/?limit=a", nil)

	h := GroupHandler{}
	h.List(c)

	assert.Equal(t, 400, rw.Code)
}

func TestGroupHandlerGetByID(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Params = []gin.Param{{Key: "id", Value: "1"}}

	m := &mockGroupService{}
	m.On("Get", &dmysc, 1).Return(&model.Group{ID: 1, Name: "Group1", CreatedAt: now, UpdatedAt: now}, nil)

	h := GroupHandler{GroupService: m}
	h.GetByID(c)

	m.AssertExpectations(t)
	exp := fmt.Sprintf(`{"data": {"id": 1, "name": "Group1", "createdAt": %s, "updatedAt": %s}}`, nowStr, nowStr)
	assert.JSONEq(t, exp, rw.Body.String())
}

func TestGroupHandlerGetByID_error(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Params = []gin.Param{{Key: "id", Value: "1"}}

	m := &mockGroupService{}
	m.On("Get", &dmysc, 1).Return((*model.Group)(nil), errors.New("an error"))

	h := GroupHandler{GroupService: m}
	h.GetByID(c)

	m.AssertExpectations(t)
	assert.Equal(t, 400, rw.Code)
	assert.JSONEq(t, `{"message": "an error"}`, rw.Body.String())
}

func TestGroupHandlerGetByID_invURLParam(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Params = []gin.Param{{Key: "id", Value: "a"}}

	h := GroupHandler{}
	h.GetByID(c)

	assert.Equal(t, 400, rw.Code)
}

func TestGroupHandlerCreate(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Request = httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"name": "Group2"}`))

	m := &mockGroupService{}
	m.On("Create", &dmysc, &model.Group{Name: "Group2"}).
		Return(&model.Group{ID: 2, Name: "Group2", CreatedAt: now, UpdatedAt: now}, nil)

	h := GroupHandler{GroupService: m}
	h.Create(c)

	m.AssertExpectations(t)
	exp := fmt.Sprintf(`{"data": {"id": 2, "name": "Group2", "createdAt": %s, "updatedAt": %s}}`, nowStr, nowStr)
	assert.JSONEq(t, exp, rw.Body.String())
}

func TestGroupHandlerCreate_error(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Request = httptest.NewRequest("POST", "/", bytes.NewBufferString("{}"))

	m := &mockGroupService{}
	m.On("Create", &dmysc, &model.Group{}).
		Return((*model.Group)(nil), errors.New("an error"))

	h := GroupHandler{GroupService: m}
	h.Create(c)

	m.AssertExpectations(t)
	assert.Equal(t, 400, rw.Code)
	assert.JSONEq(t, `{"message": "an error"}`, rw.Body.String())
}

func TestGroupHandlerCreate_invInput(t *testing.T) {
	rw := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(rw)
	c.Request = httptest.NewRequest("POST", "/", bytes.NewBufferString(`{"name": 1}`))

	h := GroupHandler{}
	h.Create(c)

	assert.Equal(t, 400, rw.Code)
}

type mockGroupService struct {
	mock.Mock
}

func (m *mockGroupService) Get(c *service.Context, id int) (*model.Group, error) {
	args := m.Called(c, id)
	return args[0].(*model.Group), args.Error(1)
}
func (m *mockGroupService) Search(c *service.Context, p *service.SearchParam) ([]*model.Group, int, error) {
	args := m.Called(c, p)
	return args[0].([]*model.Group), args.Int(1), args.Error(2)
}
func (m *mockGroupService) Create(c *service.Context, in *model.Group) (*model.Group, error) {
	args := m.Called(c, in)
	return args[0].(*model.Group), args.Error(1)
}
func (m *mockGroupService) Update(c *service.Context, in *model.Group) (*model.Group, error) {
	args := m.Called(c, in)
	return args[0].(*model.Group), args.Error(1)
}
func (m *mockGroupService) SearchUsers(c *service.Context, id int, p *service.SearchParam) ([]*model.User, int, error) {
	args := m.Called(c, id, p)
	return args[0].([]*model.User), args.Int(1), args.Error(2)
}
func (m *mockGroupService) AddUsers(c *service.Context, groupID int, userIDs []int) error {
	args := m.Called(c, groupID, userIDs)
	return args.Error(0)
}
func (m *mockGroupService) RemoveUser(c *service.Context, groupID, userID int) error {
	args := m.Called(c, groupID, userID)
	return args.Error(0)
}
