package controller

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"

	"codereview/service"
)

var newServiceContext = func(c *gin.Context) *service.Context {
	return &service.Context{
		SiteID:  "default", // dummy
		UserID:  101,       // dummy
		GroupID: 1,         // dummy

		DBDSN:        c.GetString("DSN"),
		DBLogEnabled: c.Query("_dblog") != "",
	}
}

func getURLParamInt(c *gin.Context, key string) (int, error) {
	return strconv.Atoi(c.Param(key))
}

func getQuerySearchParam(c *gin.Context) (*service.SearchParam, error) {
	searchParam := new(service.SearchParam)
	err := c.ShouldBindQuery(searchParam)
	if err != nil {
		return nil, err
	}
	return searchParam, nil
}

func errStatusCode(err error) int {
	if errors.IsNotFound(err) {
		return 404
	}
	return 400
}

func writeJSONError(c *gin.Context, err error, code int) {
	c.Error(err)
	c.JSON(code, gin.H{"message": err.Error()})
}
