package controller

import (
	"encoding/json"
	"net/http/httptest"
	"reflect"
	"runtime"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"

	"codereview/service"
)

var dmysc = service.Context{
	SiteID:  "default",
	UserID:  101,
	GroupID: 1,
}
var now time.Time
var nowStr string

func init() {
	newServiceContext = func(c *gin.Context) *service.Context {
		return &dmysc
	}
	now = time.Now()
	b, _ := json.Marshal(now)
	nowStr = string(b)
}

func TestSetRouter(t *testing.T) {
	e := gin.New()
	r := e.Group("")

	r.Use(func(c *gin.Context) {
		c.String(200, c.HandlerName()) // ハンドラ名をレスポンスで返す
		c.Abort()
	})

	h := Handler{}
	h.SetRoute(r)

	tests := []struct {
		method      string
		path        string
		wantHandler gin.HandlerFunc
	}{
		{"GET", "/users", h.User.List},
		{"GET", "/users/1", h.User.GetByID},
		{"POST", "/users", h.User.Create},
		{"PUT", "/users/1", h.User.Update},
		{"GET", "/groups", h.Group.List},
		{"GET", "/groups/1", h.Group.GetByID},
		{"POST", "/groups", h.Group.Create},
		{"PUT", "/groups/1", h.Group.Update},
		{"GET", "/groups/1/users", h.Group.UserList},
		{"POST", "/groups/1/users", h.Group.UserAdd},
		{"DELETE", "/groups/1/users/2", h.Group.UserRemove},
	}

	for _, tst := range tests {
		rw := httptest.NewRecorder()
		e.ServeHTTP(rw, httptest.NewRequest(tst.method, tst.path, nil))
		assert.Equal(t, funcName(tst.wantHandler), rw.Body.String())
	}
}

func funcName(f interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(f).Pointer()).Name()
}
