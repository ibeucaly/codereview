package controller

import (
	"github.com/gin-gonic/gin"

	"codereview/model"
	"codereview/service"
)

// GroupHandler グループ関連のハンドラ。
type GroupHandler struct {
	GroupService service.GroupService
}

// List ユーザリストを取得する。
func (h *GroupHandler) List(c *gin.Context) {
	searchParam, err := getQuerySearchParam(c)
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	groups, total, err := h.GroupService.Search(sc, searchParam)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{
		"data":  groups,
		"total": total,
	})
}

// GetByID 指定されたIDのグループを取得する。
func (h *GroupHandler) GetByID(c *gin.Context) {
	id, err := getURLParamInt(c, "id")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	user, err := h.GroupService.Get(sc, id)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{"data": user})
}

// Create グループを作成する。
func (h *GroupHandler) Create(c *gin.Context) {
	group := &model.Group{}
	err := c.ShouldBindJSON(group)
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	group, err = h.GroupService.Create(sc, group)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{"data": group})
}

// Update グループを更新する。
func (h *GroupHandler) Update(c *gin.Context) {
	id, err := getURLParamInt(c, "id")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	group := &model.Group{}
	err = c.ShouldBindJSON(group)
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	group.ID = id
	group, err = h.GroupService.Update(sc, group)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{"data": group})
}

// UserList グループに所属するユーザリストを取得する。
func (h *GroupHandler) UserList(c *gin.Context) {
	id, err := getURLParamInt(c, "id")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	searchParam, err := getQuerySearchParam(c)
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	users, total, err := h.GroupService.SearchUsers(sc, id, searchParam)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.JSON(200, gin.H{
		"data":  users,
		"total": total,
	})
}

// UserAdd グループにユーザを追加する。
func (h *GroupHandler) UserAdd(c *gin.Context) {
	id, err := getURLParamInt(c, "id")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	var param struct {
		UserIDs []int `json:"userIds"`
	}
	err = c.ShouldBindJSON(&param)
	if err != nil {
		writeJSONError(c, err, 400)
	}

	sc := newServiceContext(c)
	err = h.GroupService.AddUsers(sc, id, param.UserIDs)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.Status(200)
}

// UserRemove グループからユーザを削除する。
func (h *GroupHandler) UserRemove(c *gin.Context) {
	id, err := getURLParamInt(c, "id")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}
	userID, err := getURLParamInt(c, "userID")
	if err != nil {
		writeJSONError(c, err, 400)
		return
	}

	sc := newServiceContext(c)
	err = h.GroupService.RemoveUser(sc, id, userID)
	if err != nil {
		writeJSONError(c, err, errStatusCode(err))
		return
	}

	c.Status(200)
}
